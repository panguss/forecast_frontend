import { createRouter, createWebHistory } from "vue-router";

import LoginComponent from "@/pages/login.vue";

import RegestrationComponent from "@/pages/regestration.vue";

import WalletComponent from "@/pages/wallet.vue";

import AdminComponent from "@/pages/admin.vue";

const routes = [
  {
    path: "/login",
    name: "login",
    component: LoginComponent,
  },
  {
    path: "/regestration",
    name: "regestration",
    component: RegestrationComponent,
  },
  {
    path: "/wallet",
    name: "walet",
    component: WalletComponent,
  },
  {
    path: "/admin",
    name: "admin",
    component: AdminComponent,
  },
];

const router = createRouter({
  routes,
  history: createWebHistory(process.env.BASE_URL),
});

export default router;
